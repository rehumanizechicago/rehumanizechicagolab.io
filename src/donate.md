---
title: Support Rehumanize Chicago
eleventyNavigation:
  key: Donate
  order: 300
---

Rehumanize International currently accepts donations on behalf of 
Rehumanize Chicago via PayPal. 

To donate: 

1. Click on the "Donate with PayPal" button at https://rehumanizeintl.org/donate
2. In the PayPal page that opens, click on the drop down menu that says, "Use
this donation for" and select, "Chapters and Allies"
<img src="/img/donate/donate-to-chapters.png" alt="a screencapture of PayPal with 'Chapters and Allies' selected in the 'Use this donation for' drop down menu." class="div-img-medium" />
3. Click "Next."
4. Optionally, you may be required to log in if you are not already. 
5. On the next screen, click on "Add special instructions to the seller:" and 
type in, "Chicago."
<img class="div-img-medium" alt="a screencapture of PayPal with 'Chicago' in the box for 'Add special instructions to the seller.'" 
src="/img/donate/donate-to-chicago.png" />
6. Complete the transaction by clicking "Donate Now" at the bottom.

We can't possibly tell you how much we appreciate your help! All donations are 
tax deductible as Rehumanize is a 501(c)(3) tax exempt non-profit corporation. Donations to Rehumanize International are tax deductible. Contact Sarah Slater, Manager of Compliance and Development, sarah@rehumanizeintl.org, for a receipt recognizing your donation. Receipts for donations will be sent to donors via postal or electronic mail after the end of each fiscal year.

Rehumanize International makes its finances and IRS 990s available to the public. Find us on Guidestar, ProPublica, or contact info@rehumanizeintl.org for more information.
