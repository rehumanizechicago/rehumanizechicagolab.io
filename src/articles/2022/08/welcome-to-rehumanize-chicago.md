---
date: 2022-08-02
tags: [ "governance", "welcome" ]
title: "Welcome to Rehumanize Chicago"
---

Hello and welcome! My name is Eleanor, and I do internal communications for 
Rehumanize Chicago. We are freshly-launched consistent life ethic 
advocacy organization in 
in the Chicago metropolitan area. 

## Come to our kickoff social / watch party

Rehumanize International's Aimee Murphy has recently published a book 
about our movement and
the consistent life ethic and is 
[hosting a book launch over Zoom](https://www.eventbrite.com/e/rehumanize-book-launch-consistent-life-ethic-in-action-ft-martin-sheen-tickets-387686790627). 
She will be introducing her book and speaking with celebrated actor and 
consistent life ethic activist Martin Sheen. 

To celebrate with the broader Rehumanize community and to kick off our own 
Chicago chapter, we're hosting a watch party at 
[Cody's Public House](https://codyschicago.com/). You are cordially invited to 
watch with us and meet the rest of the Rehumanize Chicago community. 

Please [RSVP to our event on EventBrite](https://www.eventbrite.com/e/rehumanize-chicago-kickoff-social-book-launch-with-martin-sheen-tickets-395412498427). 

## The Rehumanize Book Tour is coming to Chicago on Thursday, August 25

Mark your calendars for Thursday, August 25. Aimee Murphy will be in our city
to talk about her new book *Rehumanize: A Vision to Secure Human Rights for 
All*. 

### About the book

Who deserves human rights? The answer to this question is the crux of all 
moral and political action in society, and defines our character as 
individuals and as nations. Aimee Murphy seeks to answer this vital question 
in this accessible and succinct handbook on the Consistent Life Ethic, a 
moral philosophy whose central principle is that each and every human being 
has inherent dignity from conception to natural death, and therefore deserves 
to live free from violence. Rehumanize: A Vision to Secure Human Rights for 
All includes a digestible yet systematic analysis of the history, ethics, and 
public policy surrounding modern issues of dehumanization, and casts a 
rehumanizing vision of a world beyond violence. Beyond the confines of 
political parties or religious exclusion, the founder of Rehumanize 
International communicates an aspiration to an inclusive ideal of a 
consistent movement of every human standing for every human. Activists, 
scholars, and leaders young and old will find this resource an indispensable 
cornerstone for their outreach and advocacy for decades to come.

## Rehumanize International Conference will be virtual on October 15

Rehumanize International is meeting virtual again this year via Hopin on 
October 15. Consider applying to be a speaker or buy your ticket for the
virtual event. 

I intend to organize an in-person watching, so mark your calendars. 

[Learn more about the conference.](https://www.rehumanizeintl.org/conference)

## Send us your events!

Have an event you think we should co-sponsor or even just attend? We're open
to collaborating with organizations working against legalized or normalized
violence. 

## Chicago News

* [City Agencies Can’t Help Other States Investigate People Who Come To 
Chicago For Abortions, Lightfoot 
Says](https://blockclubchicago.org/2022/07/28/city-agencies-cant-help-other-states-investigate-people-who-come-to-chicago-for-abortions-lightfoot-says/), via
Block Club

* [Oak Lawn police release dashboard camera video of arrest of 17-year-old 
left hospitalized](https://www.chicagotribune.com/10a2f71a-fe3d-4927-a810-27a6f61c586d-132.html), via the Chicago Tribune (warning: not easy to watch)

* [Teen beaten by police in Oak Lawn released from hospital, taken to juvenile detention](https://chicago.suntimes.com/news/2022/8/1/23287984/teen-police-oak-lawn-juvenile-detention-videotape), via the Chicago Sun-Times

See any local news you think Chicago region consistent lifers should read? 
Send it in!

## Follow us on social media!

If you haven't already, please consider 
[liking us on Facebook](https://www.facebook.com/rehumanizechi/), 
[following us on Twitter](https://twitter.com/rehumanizechi/), or. Even if you
don't use social media often, having just a few more people following us helps
legitimize us to the algorithms, and we could really use the boost. 



