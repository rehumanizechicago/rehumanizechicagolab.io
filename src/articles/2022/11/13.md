---
date: 2022-11-13
tags: ["election", "police-brutality", "immigration", "prenatal-rights", "racism", "treatment-not-trauma" ]
title: "The Chicago Civic Update, Post-Election Round-Up, and Other News."
---

### The Chicago Civic Update

Our friends at And Campaign Chicago have launched their own newsletter called
[The Chicago Civic Update](https://chicagocivicupdate.substack.com/) on 
Substack. The [And Campaign](https://andcampaign.org/where-we-stand/) is a 
Christian consistent life movement with chapters across the country. 

Rehumanize is a secular organization, but we're excited to promote consistent
life work from any religious group in the region and encourage our followers
who are Christians to check them out!

### Consistent Life News

*We collect stories of sadly normalized violence around the metropolitan area.
See something we should see? Tag us or send it in.*

- [Chicago Police Board votes to fire officer who fatally shot apparently unarmed man in 2018](https://chicago.suntimes.com/crime/2022/9/15/23355908/chicago-police-board-vote-fire-officer-sheldon-thrasher), via Chicago Sun-Times.
- [Anjanette Young Push To Change Search Warrant Policy Fails To Get Aldermanic Support](https://blockclubchicago.org/2022/11/11/anjanette-young-push-to-change-search-warrant-policy-fails-to-get-aldermanic-support/), via Block Club.
- [Immigration advocates celebrate election firsts, plan next steps for reform](https://chicago.suntimes.com/2022/11/9/23449746/immigration-immigrants-illinois-legislature-elections-general-assembly-springfield-korea-vietnam), via Chicago Sun-Times.
- [Chicago Women Head To The Polls To Protect Right To Abortion: ‘We Can’t Afford To Go Back’](https://blockclubchicago.org/2022/11/09/chicago-women-head-to-the-polls-to-protect-their-right-to-an-abortion-in-illinois-we-cant-afford-to-go-back/), via Block Club.
- [General Iron’s Lincoln Park Facility Will Soon Be Demolished; Alderman Vows No Repeat Of Hilco Disaster](https://blockclubchicago.org/2022/11/09/general-irons-lincoln-park-facility-will-soon-be-demolished-alderman-vows-no-repeat-of-hilco-disaster/), via Block Club. (Note: We usually don't cover environmental stories not because they are not important but because they're outside our core mission, but the story of the General Iron plant has become more than that--it's a story about racism and civil rights, which is why it is included here.)
- [Flight Attendants Urge Lawmakers To Better Protect Them From Passenger Abuse](https://blockclubchicago.org/2022/11/11/flight-attendants-urge-lawmakers-to-better-protect-them-from-passenger-abuse/), via Block Club.
- [Voters Want To Reopen City’s Closed Mental Health Clinics. An Alderperson Says She’ll Use Momentum To Revive Stalled Plan](https://blockclubchicago.org/2022/11/11/voters-want-to-reopen-citys-closed-mental-health-clinics-an-alderperson-says-shell-use-momentum-to-make-it-happen/), via Block Club.
- [Obama Center Builders Halt Construction After Noose Found On Site](https://blockclubchicago.org/2022/11/10/obama-center-builders-halt-construction-after-noose-found-on-site/), via Block Club.
