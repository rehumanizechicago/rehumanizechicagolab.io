---
date: 2024-12-02
tags: [ "giving-tuesday", "mutual-aid", "prenatal-rights", "suburbs", "torture" ]
title: "CTJMF Fundraiser Tomorrow, Founding a Suburban Mutual Aid Network, and an Open Officer Position"
---

### Join us for the Chicago Torture Justice Memorial Foundation Fundraiser Tomorrow

We are proud to be cosponsors of the the [Chicago Torture Justice Memorial
Foundation](https://ctjmfoundation.org/about/)'s 
["Making History, Forging a New Future" fundraising 
event](https://ctjmf.square.site/product/general-admission-making-history-forging-a-new-future-2024/26)!
Please join us
**tomorrow, Tuesday, December 3, from 6pm-9pm at Overflow Coffee at 1449 S. Michigan
Ave.** It will be nice 
to have several Consistent Life Ethic friends there to
celebrate and support reparations for and education about torture by police
in Chicago.

If you're interested in joining us, please 
[buy a ticket](https://ctjmf.square.site/product/general-admission-making-history-forging-a-new-future-2024/26), 
and to coordinate to sit with us, send an email to 
[secretary@rehumanizechicago.org](mailto:secretary@rehumanizechicago.org) or
reach out to us [on Keybase](https://keybase.io/team/rehumanizechi/) or 
through any of our social links listed at the bottom. 

### Exploring Founding a Suburban Mutual Aid Network

Mutual aid networks connect people with material needs to resources in their
communities. We're admire their important work, but, as one of our members has
pointed out, they often explicitly support elective induced abortion.

We're exploring the possibility of creating a life-affirming mutual aid network
to serve the suburbs. We don't have a lot of news to share at the moment, but
**if you have any interest in helping such a group, please reach out through
any channel** and we'll get you connected up! We're not yet committed to any
one idea of how this should look, so if you have thoughts now would be a good
time to get involved and share. 

### Open volunteer position in Rehumanize Chicago

We have an open volunteer position! If you're looking to maximize your impact
on the consistent life ethic movement with just a *tiny* time commitment, this
is a good opportunity for you. We need a treasurer role filled--wait, don't
scroll away! Rehumanize International handles all the hard stuff. We have 
a *very* small amount of money available to us through Rehumanize 
International and *no* regular expenses. 

This is a very *important* role to have filled for accountability reasons, but
really the commitment involved amounts to checking the bottom line 
on a spreadsheet every few
months or so. **It would be an excellent opportunity for someone who has 
trouble making regular events or needs a lot of flexibility in a volunteer 
opportunity.**

If you have any interest or any questions about the position of treasurer, 
please reach out
through any channel, and by all means if you know a consistent lifer who might
be interested, please pass this on. 

### The Dreaded Giving Tuesday

I rarely solicit donations here for a number of reasons. Primarily, I want 
folks to trust that we aren't going to spam them with repeated requests for 
monetary support. I want people to know every time they open one of these 
emails (at-most monthly!) or follow any of our social accounts that it's full 
of useful, skimmable, actionable information. 

Another reason is that, frankly, we have very little need for money. This is 
an operation that runs on manpower much more than dollars. 

But I figure you'll forgive me if, once a year, I make a brief pitch for
Rehumanize Chicago in honor of Giving Tuesday, and it is this: **Rehumanize 
Chicago is an incredibly efficient local 501c3 charity.** We have no 
employees. We have no building with rent and utilities to support. Even our 
website is
hosted for free. If you decide to donate to Rehumanize Chicago, you can be 
confident your entire tax-deductible donation will go to our most direct needs
in the mission of promoting the consistent life ethic and opposing violence
in the greater Chicago metropolitan area. 

**If you would like to donate, please follow the 
[instructions on our website](/donate/). We accept PayPal.**

### Join our Keybase Chat

Sometimes an opportunity to promote life and oppose violence comes up on short
notice, and we don't have time to crank out a newsletter. If you want a more 
immediate way to hear from us, 
[please join our Keybase chat](https://keybase.io/team/rehumanizechi/). 
Introduce yourself, make friends, and share opportunities we should help with.

### Follow us on social media

Following us on social media is a great way to keep up with what we're up to, 
and interacting with our posts will help other people discover us. 
Give us a boost!

* [Bluesky](https://bsky.app/profile/rehumanizechicago.org)
* [Facebook](https://www.facebook.com/rehumanizechi/)
* [Instagram](https://www.instagram.com/rehumanizechi/)
* [Mastodon](https://mstdn.social/@RehumanizeChi)
* [Threads](https://www.threads.net/@rehumanizechi)
* [Twitter/X](https://x.com/RehumanizeChi)
