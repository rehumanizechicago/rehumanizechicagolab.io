let
  # nixpkgs-24.11-darwin
  # 2025-03-01
  nixpkgs = fetchTarball "https://github.com/NixOS/nixpkgs/archive/26a98b6560fe0526b324d76619ffc55e61b4625e.tar.gz";
  pkgs = import nixpkgs { config = {}; overlays = []; };
in

pkgs.mkShellNoCC {
  packages = with pkgs; [
    cacert 
    git
    less
    nodejs_23
  ];
}
