{ buildNpmPackage, lib }:
let
  fs = lib.fileset;
  sourceFiles = fs.gitTracked ./.;
in
buildNpmPackage rec {
  npmDepsHash = "sha256-+4lLBQ+UQ2XT0wwE6jADxG1UNZjLkQCLvvN1SdiUwZY=";
  pname = "rehumanizechicago";
  src = fs.toSource {
    root = ./.;
    fileset = sourceFiles;
  };
  postInstall = ''
cp -rv public/ $out
'';
  version = "0.0.1";
}
