const { DateTime } = require("luxon");
const fs = require("fs");
const pluginRss = require("@11ty/eleventy-plugin-rss");
const pluginSyntaxHighlight = require("@11ty/eleventy-plugin-syntaxhighlight");
const pluginNavigation = require("@11ty/eleventy-navigation");
const markdownIt = require("markdown-it");
const markdownItAnchor = require("markdown-it-anchor");

const OUTPUT_DIR = "public/";
const MARKDOWN_OPTIONS =
      {
          html: true,
          breaks: false,
          linkify: true
      };

module.exports = function(eleventyConfig) {
    eleventyConfig.addPlugin(pluginRss);
    eleventyConfig.addPlugin(pluginSyntaxHighlight);
    eleventyConfig.addPlugin(pluginNavigation);    
    eleventyConfig.setDataDeepMerge(true);
    eleventyConfig.setFrontMatterParsingOptions({ excerpt: true
                                                });
    eleventyConfig.addLayoutAlias("post", "layouts/post.njk");

    /* takes the collections object and returns a list of tags sorted by 
     * how many articles it has. */
    eleventyConfig.addFilter("byImportance", collections => {
        return collections.tagList
            .sort((tag1, tag2) =>
                  collections[tag2].length - collections[tag1].length
                 );
    });

    /* takes list of events and returns future events */
    eleventyConfig.addFilter("future", events => {
	      return events.filter(event => {
	          return event.date >= new Date();
        });
    });

    eleventyConfig.addFilter("readableDate", dateObj => {
        return DateTime.fromJSDate(dateObj, {zone: 'utc'})
            .toFormat("dd LLL yyyy");
    });

  // https://html.spec.whatwg.org/multipage/common-microsyntaxes.html#valid-date-string
  eleventyConfig.addFilter('htmlDateString', (dateObj) => {
    return DateTime.fromJSDate(dateObj, {zone: 'utc'}).toFormat('yyyy-LL-dd');
  });

  // Get the first `n` elements of a collection.
  eleventyConfig.addFilter("head", (array, n) => {
    if( n < 0 ) {
      return array.slice(n);
    }

    return array.slice(0, n);
  });

    eleventyConfig.addCollection("tagList", require("./_11ty/getTagList"));
    
    eleventyConfig.addPassthroughCopy("img");
    eleventyConfig.addPassthroughCopy("css");
    eleventyConfig.addPassthroughCopy("manifest.json");
    eleventyConfig.addPassthroughCopy("scripts");
    eleventyConfig.addPassthroughCopy("SW.bs.js");
    /* Markdown Overrides */
    let markdownLibrary = markdownIt(MARKDOWN_OPTIONS);
    eleventyConfig.setLibrary("md", markdownLibrary);
    eleventyConfig.addFilter("toHTML", str => {
        return new markdownIt(MARKDOWN_OPTIONS).renderInline(str);
    });


  // Browsersync Overrides
  eleventyConfig.setBrowserSyncConfig({
    callbacks: {
      ready: function(err, browserSync) {
        const content_404 = fs.readFileSync(OUTPUT_DIR + '404.html');

        browserSync.addMiddleware("*", (req, res) => {
          // Provides the 404 content without redirect.
          res.write(content_404);
          res.end();
        });
      },
    },
    ui: false,
    ghostMode: false
  });

  return {
    templateFormats: [
      "md",
      "njk",
      "html",
      "liquid"
    ],

    // If your site lives in a different subdirectory, change this.
    // Leading or trailing slashes are all normalized away, so don’t worry about those.

    // If you don’t have a subdirectory, use "" or "/" (they do the same thing)
    // This is only used for link URLs (it does not affect your file structure)
    // Best paired with the `url` filter: https://www.11ty.dev/docs/filters/url/

    // You can also pass this in on the command line using `--pathprefix`
    // pathPrefix: "/",

    markdownTemplateEngine: "njk",
    htmlTemplateEngine: "njk",
    dataTemplateEngine: "njk",

    // These are all optional, defaults are shown:
    dir: {
      input: "src",
      includes: "_includes",
      data: "_data",
      output: OUTPUT_DIR
    }
  };
};
